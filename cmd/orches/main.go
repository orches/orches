package main

import (
	"fmt"
	"os"

	initCmd "gitlab.com/orches/orches/internal/cmd/init"
	"gitlab.com/orches/orches/internal/cmd/orches"
	"gitlab.com/orches/orches/internal/cmd/version"
)

var rootCmd = orches.Cmd

func init() {
	rootCmd.AddCommand(version.Cmd)
	rootCmd.AddCommand(initCmd.Cmd)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
