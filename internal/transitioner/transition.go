package transitioner

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"runtime"

	"gitlab.com/orches/orches/internal/state"
)

type Result struct {
	RestartOrches bool
}

func updateOrches(orches state.Orches) error {
	f, err := os.CreateTemp("/usr/local/bin", "")
	if err != nil {
		return err
	}

	// this might fail and this is fine, the file might not be there
	defer os.Remove(f.Name())

	resp, err := http.Get(fmt.Sprintf("%s/builds/commits/%s/%s/orches", orches.Origin, orches.Commit, runtime.GOARCH))
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("orches download server returned %d", resp.StatusCode)
	}

	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return err
	}

	err = f.Close()
	if err != nil {
		return err
	}

	err = os.Chmod(f.Name(), 0700)
	if err != nil {
		return err
	}

	return os.Rename(f.Name(), "/usr/local/bin/orches")
}

func Transition(old *state.State, new *state.State) (*state.State, *Result, error) {
	var result Result
	current := old.Clone()

	if old.Orches != new.Orches {
		err := updateOrches(new.Orches)
		if err != nil {
			return current, &result, err
		}

		current.Orches = new.Orches
		result.RestartOrches = true

		return current, &result, nil
	}

	return current, &result, nil
}
