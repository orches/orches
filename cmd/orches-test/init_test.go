//go:build integration
// +build integration

package orches_test

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"path"
	"runtime"
	"testing"
	"time"

	"github.com/coreos/go-systemd/v22/dbus"
	"github.com/stretchr/testify/require"

	"gitlab.com/orches/orches/internal/common"
)

func initDownloadServer(t *testing.T) *httptest.Server {
	fileServerDir := t.TempDir()
	buildDir := path.Join(fileServerDir, "builds/commits", common.Commit(), runtime.GOARCH)
	err := os.MkdirAll(buildDir, 0755)
	require.NoError(t, err)

	err = common.CopyFile(path.Join("bin", runtime.GOARCH, "orches"), path.Join(buildDir, "orches"), 0644)
	require.NoError(t, err)

	server := httptest.NewServer(http.FileServer(http.Dir(fileServerDir)))

	t.Cleanup(server.Close)

	return server
}

func cleanOrches(t *testing.T) func() {
	return func() {
		ctx := context.Background()
		c, err := dbus.NewWithContext(ctx)
		require.NoError(t, err)

		_, err = c.StopUnitContext(ctx, "orches.service", "replace", nil)
		require.NoError(t, err)

		_, err = c.DisableUnitFilesContext(ctx, []string{"orches.service"}, false)
		require.NoError(t, err)

		err = os.Remove("/usr/local/bin/orches")
		require.NoError(t, err)

		err = os.RemoveAll("/var/lib/orches")
		require.NoError(t, err)
	}
}

func TestInit(t *testing.T) {
	repo := t.TempDir()
	cmd := exec.Command("git", "-C", repo, "init", "--bare")
	err := cmd.Run()
	require.NoError(t, err)

	server := initDownloadServer(t)

	cmd = exec.Command(path.Join("bin", runtime.GOARCH, "orches"), "init", "--remote", repo, "--origin", server.URL)
	output, err := cmd.CombinedOutput()
	fmt.Println(string(output))
	require.NoError(t, err)

	t.Cleanup(cleanOrches(t))

	time.Sleep(1 * time.Second)

	ctx := context.Background()
	c, err := dbus.NewWithContext(ctx)
	require.NoError(t, err)

	state, err := c.GetUnitPropertyContext(ctx, "orches.service", "ActiveState")
	require.NoError(t, err)

	require.Equal(t, "active", state.Value.Value().(string))

}
