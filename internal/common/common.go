package common

import (
	_ "embed"
	"io"
	"log"
	"os"
	"strings"
)

//go:embed commit
var commit string

func Commit() string {
	return strings.TrimSpace(commit)
}

func LogError(f func() error) {
	err := f()
	if err != nil {
		log.Print(err.Error())
	}
}

func CopyFile(src, dest string, perm os.FileMode) error {
	srcFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer LogError(srcFile.Close)

	destFile, err := os.OpenFile(dest, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, perm)
	if err != nil {
		return err
	}
	defer LogError(destFile.Close)

	_, err = io.Copy(destFile, srcFile)
	return err
}
