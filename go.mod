module gitlab.com/orches/orches

go 1.16

require gopkg.in/yaml.v2 v2.4.0

require (
	github.com/coreos/go-systemd/v22 v22.3.2
	github.com/kr/text v0.2.0 // indirect
	github.com/spf13/cobra v1.5.0
	github.com/stretchr/testify v1.8.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
