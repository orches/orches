package repository

import (
	"gitlab.com/orches/orches/internal/state"
)

type Repository interface {
	InitNamespace(string) error

	Load(namespace string, s *state.State) error
	Save(namespace string, s *state.State) error
	Copy(srcNamespace string, dstNamespace string) error
	Update(namespace string) error
}
