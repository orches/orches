package orches

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"

	"gitlab.com/orches/orches/internal/common"
	"gitlab.com/orches/orches/internal/model"
	"gitlab.com/orches/orches/internal/repository/git"
)

var Cmd = &cobra.Command{
	Use:   "orches",
	Short: "Orches is a simple container orchestrator",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Running orches " + common.Commit())

		repository := git.Init("/var/lib/orches/repository")

		m, err := model.Init(repository)
		if err != nil {
			log.Fatal(err.Error())
		}

		for {
			fmt.Println("Running periodic sync.")
			result, err := m.TransitionFromRepository()
			if err != nil {
				log.Fatal(err)
			}
			if result == nil {
				fmt.Println("Nothing to update.")
			} else if result != nil && result.RestartOrches {
				fmt.Println("Orches reference updated, restarting.")
				os.Exit(0)
			} else {
				fmt.Println("Deployments updated.")
			}

			time.Sleep(5 * time.Minute)
		}
	},
}
