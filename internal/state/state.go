package state

type State struct {
	Orches Orches
}

type Orches struct {
	Commit string
	Origin string
}

func (s State) Clone() *State {
	return &State{
		Orches: s.Orches,
	}
}

func (s State) Equals(o *State) bool {
	return s.Orches == o.Orches
}
