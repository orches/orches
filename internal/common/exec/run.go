package exec

import (
	"fmt"
	"os/exec"
)

type Error struct {
	Err    error
	Output []byte
}

func (e *Error) Error() string {
	return fmt.Sprintf("%v\noutput:\n%s\n", e.Err, string(e.Output))
}

func (e *Error) Unwrap() error {
	return e.Err
}

func Run(name string, arg ...string) error {
	cmd := exec.Command(name, arg...)

	output, err := cmd.CombinedOutput()
	if _, ok := err.(*exec.ExitError); ok {
		return &Error{
			Err:    err,
			Output: output,
		}
	}

	return err
}
