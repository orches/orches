package model

import (
	"gitlab.com/orches/orches/internal/repository"
	"gitlab.com/orches/orches/internal/state"
	"gitlab.com/orches/orches/internal/transitioner"
)

type Model struct {
	repository repository.Repository
	current    *state.State
}

const (
	currentNamespace = "current"
	goalNamespace    = "goal"
)

func Init(r repository.Repository) (*Model, error) {
	var current state.State
	err := r.Load(currentNamespace, &current)
	if err != nil {
		return nil, err
	}

	return &Model{
		repository: r,
		current:    &current,
	}, nil
}

func InitEmpty(r repository.Repository) (*Model, error) {
	err := r.InitNamespace(currentNamespace)
	if err != nil {
		return nil, err
	}

	err = r.Copy(currentNamespace, goalNamespace)
	if err != nil {
		return nil, err
	}

	m := Model{
		repository: r,
		current:    &state.State{},
	}

	return &m, nil
}

func (m Model) CurrentState() *state.State {
	return m.current.Clone()
}

func (m Model) doTransition(goal *state.State) (*transitioner.Result, error) {
	current, result, err := transitioner.Transition(m.current, goal)
	if err != nil {
		// save current?
		return nil, err
	}

	m.current = current

	if m.current.Equals(goal) {
		err = m.repository.Copy(goalNamespace, currentNamespace)
		if err != nil {
			return nil, err
		}
	} else {
		err = m.repository.Save(currentNamespace, m.current)
		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

func (m Model) TransitionToState(goal *state.State) (*transitioner.Result, error) {
	if m.current.Equals(goal) {
		return nil, nil
	}

	err := m.repository.Save(goalNamespace, goal)
	if err != nil {
		return nil, err
	}

	return m.doTransition(goal)
}

func (m Model) TransitionFromRepository() (*transitioner.Result, error) {
	err := m.repository.Update(goalNamespace)
	if err != nil {
		return nil, err
	}

	var goal state.State
	err = m.repository.Load(goalNamespace, &goal)
	if err != nil {
		return nil, err
	}

	if m.current.Equals(&goal) {
		return nil, nil
	}

	return m.doTransition(&goal)
}
