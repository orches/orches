build-amd64:
	git rev-parse HEAD >internal/common/commit
	mkdir -p bin/amd64
	GOARCH=amd64 go build -o bin/amd64/orches ./cmd/orches
	GOARCH=amd64 go test -c --tags integration -o bin/amd64/orches-test ./cmd/orches-test
	echo "dev" >internal/common/commit

build-arm64:
	git rev-parse HEAD >internal/common/commit
	mkdir -p bin/arm64
	GOARCH=arm64 go build -o bin/arm64/orches ./cmd/orches
	GOARCH=arm64 go test -c --tags integration -o bin/arm64/orches ./cmd/orches-test
	echo "dev" >internal/common/commit

build: build-amd64 build-arm64

install: build-amd64
	mv bin/amd64/orches /usr/local/bin/
	chmod +x /usr/local/bin/orches
	sudo /usr/local/bin/orches init --remote "${REPOSITORY}" --ssh-key "${SSH_KEY}"

.PHONY: build build-amd64 build-arm64 install
