#!/bin/sh

die_func() {
    kill %1
    exit 0
}
trap die_func TERM INT

sleep infinity &
wait
