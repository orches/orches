# Orches - simple container orchestration
Goals:
- container orchestrator for a single node (not yet)
- built-in GitOps (in progress)
- built-in secret management (not yet)
- built-in backups (not yet)

## Installation
Create a repository with the following `orches.yaml` file inside:
```yaml
orches:
  commit: abcdef
```

Run:
```
curl -sfL https://gitlab.com/orches/orches/-/raw/main/install.sh | sh -s - git@gitlab.com:orches/example.git PATH_TO_SSH_KEY
```

After this command is finished, orches should be running as a service and the
installed version should be the same as specified in `orches.yaml`. You can
verify with:

```
systemctl status orches.service
/usr/local/bin/orches version
```
