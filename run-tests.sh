#!/bin/bash

set -x

podman build -t orches-test -f test/Containerfile . >/dev/null
CID=$(podman run --rm -d orches-test)

die_func() {
    podman stop "$CID" >/dev/null
}
trap die_func TERM INT EXIT

# TODO: make this multi-platform
podman exec "$CID" make build-amd64
podman exec "$CID" bin/amd64/orches-test
