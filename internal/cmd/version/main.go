package version

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab.com/orches/orches/internal/common"
)

var Cmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of Orches",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(common.Commit())
	},
}
