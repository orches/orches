package init

import (
	_ "embed"
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/orches/orches/internal/common"
	"gitlab.com/orches/orches/internal/common/exec"
	"gitlab.com/orches/orches/internal/model"
	"gitlab.com/orches/orches/internal/repository/git"
)

//go:embed orches.service
var serviceFile []byte

var remote string
var sshKey string
var origin string

const defaultOrigin = "https://f003.backblazeb2.com/file/orches"

var Cmd = &cobra.Command{
	Use:   "init",
	Short: "Initialize Orches service",
	Run: func(cmd *cobra.Command, args []string) {

		err := os.MkdirAll("/var/lib/orches", 0755)
		if err != nil {
			log.Fatal(err.Error())
		}

		var keyCopy string
		if sshKey != "" {
			// copy the key only if it was given to us
			keyCopy = "/var/lib/orches/ssh-key"
			err = common.CopyFile(sshKey, keyCopy, 0400)
			if err != nil {
				log.Fatal(err.Error())
			}
		}

		repository, err := git.InitEmpty("/var/lib/orches/repository", keyCopy, remote)
		if err != nil {
			log.Fatal(err.Error())
		}

		m, err := model.InitEmpty(repository)
		if err != nil {
			log.Fatal(err.Error())
		}

		goal := m.CurrentState()
		goal.Orches.Commit = common.Commit()
		goal.Orches.Origin = origin

		_, err = m.TransitionToState(goal)
		if err != nil {
			log.Fatal(err.Error())
		}

		err = initService()
		if err != nil {
			log.Fatal(err.Error())
		}
	},
}

func init() {
	Cmd.Flags().StringVar(&remote, "remote", "", "URL of a git remote used to store the state.")
	Cmd.MarkFlagRequired("remote")

	Cmd.Flags().StringVar(&sshKey, "ssh-key", "", "Path to a private ssh key used to connect to the git remote.")
	Cmd.Flags().StringVar(&origin, "origin", defaultOrigin, "Base URL of an Orches download server.")
}

func writeServiceFile() error {
	f, err := os.OpenFile("/etc/systemd/system/orches.service", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer common.LogError(f.Close)

	_, err = f.Write(serviceFile)
	return err
}

func initService() error {
	err := writeServiceFile()
	if err != nil {
		return err
	}

	err = exec.Run("systemctl", "daemon-reload")
	if err != nil {
		return err
	}

	err = exec.Run("systemctl", "restart", "orches.service")
	if err != nil {
		return err
	}

	err = exec.Run("systemctl", "enable", "orches.service")
	if err != nil {
		return err
	}

	return nil
}
