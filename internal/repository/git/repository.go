package git

import (
	"errors"
	"os"
	"path"

	"gopkg.in/yaml.v2"

	"gitlab.com/orches/orches/internal/common/exec"
	"gitlab.com/orches/orches/internal/repository"
	"gitlab.com/orches/orches/internal/state"
)

type gitRepo struct {
	dir string
}

const remote = "origin"

func (r gitRepo) run(args ...string) error {
	defaultArgs := []string{"-C", r.dir}
	return exec.Run("git", append(defaultArgs, args...)...)
}

func Init(dir string) repository.Repository {
	return gitRepo{dir: dir}
}

func InitEmpty(dir, key, remoteURL string) (repository.Repository, error) {
	_, err := os.Stat(dir)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return nil, err
	}
	if err == nil {
		return nil, errors.New("repository exists")
	}

	err = os.Mkdir(dir, 0755)
	if err != nil {
		return nil, err
	}

	r := gitRepo{
		dir: dir,
	}

	err = r.run("init", "-b", "_orches_system")
	if err != nil {
		return nil, err
	}

	err = r.run("config", "user.name", "Orches")
	if err != nil {
		return nil, err
	}

	err = r.run("config", "user.email", "orches@budai.cz")
	if err != nil {
		return nil, err
	}

	if key != "" {
		err = r.run("config", "core.sshCommand", "ssh -i "+key)
		if err != nil {
			return nil, err
		}
	}

	err = r.run("remote", "add", remote, remoteURL)
	if err != nil {
		return nil, err
	}

	return r, nil
}

type orchesFile struct {
	Orches struct {
		Commit string `yaml:"commit"`
		Origin string `yaml:"origin"`
	} `yaml:"orches"`
}

func (r gitRepo) InitNamespace(namespace string) error {
	err := r.run("checkout", "--orphan", namespace)
	if err != nil {
		return err
	}

	// Try to remove everything. Will fail if the worktree is already empty
	// so ignore the error.
	_ = r.run("rm", "-rf", ".")

	err = writeReadme(path.Join(r.dir, "README.md"))
	if err != nil {
		return err
	}

	err = r.run("add", "README.md")
	if err != nil {
		return err
	}

	err = r.run("commit", "-m", "Initialize Orches deployment repository")
	if err != nil {
		return err
	}

	return r.run("push", remote, namespace)
}

func (r gitRepo) Load(namespace string, s *state.State) (err error) {
	err = r.run("checkout", namespace)
	if err != nil {
		return
	}

	f, err := os.Open(path.Join(r.dir, "orches.yaml"))
	if err != nil {
		return
	}
	defer func() {
		e := f.Close()
		if e != nil && err == nil {
			err = e
		}
	}()

	decoder := yaml.NewDecoder(f)
	decoder.SetStrict(true)

	var of orchesFile
	err = decoder.Decode(&of)
	if err != nil {
		return
	}

	s.Orches.Commit = of.Orches.Commit
	s.Orches.Origin = of.Orches.Origin
	return
}

func (r gitRepo) Save(namespace string, s *state.State) (err error) {
	err = r.run("checkout", namespace)
	if err != nil {
		return
	}

	f, err := os.Create(path.Join(r.dir, "orches.yaml"))
	if err != nil {
		return
	}
	defer func() {
		e := f.Close()
		if e != nil && err == nil {
			err = e
		}
	}()

	encoder := yaml.NewEncoder(f)

	var of orchesFile
	of.Orches.Commit = s.Orches.Commit
	of.Orches.Origin = s.Orches.Origin

	err = encoder.Encode(of)
	if err != nil {
		return
	}

	err = r.run("add", ".")
	if err != nil {
		return
	}

	err = r.run("commit", "-m", "Update state")
	if err != nil {
		return
	}

	err = r.run("push", remote, namespace)
	return
}

func (r gitRepo) Copy(srcNamespace string, dstNamespace string) error {
	err := r.run("checkout", dstNamespace)
	if err != nil {
		err = r.run("checkout", "-b", dstNamespace)
		if err != nil {
			return err
		}
	}

	err = r.run("reset", "--hard", srcNamespace)
	if err != nil {
		return err
	}

	return r.run("push", "--force", remote, dstNamespace)
}

func (r gitRepo) Update(namespace string) error {
	err := r.run("checkout", namespace)
	if err != nil {
		return err
	}
	return r.run("pull", remote, namespace)
}

func writeReadme(filename string) (err error) {
	f, err := os.Create(filename)
	if err != nil {
		return
	}
	defer func() {
		if e := f.Close(); err == nil && e != nil {
			err = e
		}
	}()

	_, err = f.Write([]byte("# Orches deployment repository\n"))

	return
}
